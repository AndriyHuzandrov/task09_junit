package com.nexus6.task09;

import com.nexu6.task09.control.GameDriver;
import java.lang.reflect.Field;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameDriverTests {
  @InjectMocks
  private GameDriver driverMock;

  @Test
  public void SetFieldTrue () throws Exception {
    Field fToTest = driverMock.getClass().getDeclaredField("isFieldCreated");
    fToTest.setAccessible(true);
    driverMock.createMineField(10, 10, 0.3);
    Assert.assertTrue(fToTest.getBoolean(driverMock));
  }

}
