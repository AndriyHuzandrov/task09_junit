package com.nexu6.task09.model;

import java.util.Random;

public class MineField {
  private String[][] mineLocArr;
  private int[][] gameplayArr;
  private double prob;
  int xSize;
  int ySize;

  public MineField(int x, int y, double p) {
    xSize = x;
    ySize = y;
    prob = p;
    mineLocArr = new String[y][x];
    gameplayArr = new int[y][x];
  }

  private void deployMines() {
    String mark;
    for(int i = 0; i < ySize; i++) {
      int minesInLine = (int) Math.round(prob * xSize);
      for(int j = 0; j < xSize; j++) {
        mark = (isMine() && --minesInLine > 0) ? "* " : "- ";
        mineLocArr[i][j] = mark;
      }
    }
  }
  private boolean isMine() {
    Random rnd = new Random();
    return (rnd.nextInt(2) == 1);
  }

  private void minesToNums() {
    for(int j = 0; j < ySize; j++) {
      for(int i  = 0; i < xSize; i++) {
        if(mineLocArr[j][i].equals("* ")) {
          if(j == 0) {
            markHoriz(1, i, j);
          } else if(j == (ySize - 1)) {
            markHoriz(-1, i, j);
          } else {
            markHoriz(-1, i, j);
            markHoriz(1, i ,j);
          }
          markVert(i, j);
        }
      }
    }
  }
  private void markHoriz(int displacement, int currX, int currY) {
    for(int i = currX - 1; i <= currX + 1; i++) {
      if(currX == 0) {
        continue;
      }
      if(!mineLocArr[currY + displacement][i].equals("* ")) {
        gameplayArr[currY + displacement][i] += 1;
      }
      if(i == xSize) {
        break;
      }
    }
  }
  private void markVert(int currX, int currY) {
    if(currX > 0 && currX < xSize) {
      gameplayArr[currY][currX - 1] += 1;
      gameplayArr[currY][currX + 1] += 1;
    }
  }

  public int[][] getGameplayArr() {
    minesToNums();
    return gameplayArr;
  }

  public String[][] getMineLocArr() {
    deployMines();
    return mineLocArr;
  }
}
