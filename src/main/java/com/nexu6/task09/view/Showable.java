package com.nexu6.task09.view;

public interface Showable {
  void show(String s);
}
