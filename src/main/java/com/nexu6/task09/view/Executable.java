package com.nexu6.task09.view;

@FunctionalInterface
public interface Executable {
  void execute();
}
