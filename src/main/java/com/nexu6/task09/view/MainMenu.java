package com.nexu6.task09.view;

import com.nexu6.task09.control.FieldNotCreatedException;
import com.nexu6.task09.control.GameDriver;
import com.nexu6.task09.utils.TxtConst;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu {
  private BufferedReader br;
  private GameDriver driver;
  private Map<String, Executable> execMenu;

  public MainMenu() {
    br = new BufferedReader(new InputStreamReader(System.in));
    driver = new GameDriver();
    execMenu = new LinkedHashMap<>();
    prepareExecMenu();
  }

  public void getChoice() {
    showTxtMenu();
    int txtMenuBound = TxtConst.txtMenu.length - 1;
    try {
      String choice = br.readLine();
      if(choice.matches("[0-"+ txtMenuBound +"]")) {
        processChoice(choice);
      } else {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      TxtConst.appLog.error(TxtConst.INPUT_ERR_MSG);
    } catch (IllegalArgumentException  e) {
      TxtConst.appLog.warn(TxtConst.BAD_MENU_ITM_MSG);
      getChoice();
    } catch (FieldNotCreatedException e) {
      TxtConst.appLog.warn(e);
    }
  }

  private void prepareExecMenu(){
    execMenu.put("0", this::exitApp);
    execMenu.put("1", this::createField);
    execMenu.put("2", this::showMinesLocation);
    execMenu.put("3", this::showGameMap);
  }

  private void showGameMap() {
    TxtConst.appLog.info(driver.showMineLoc());
  }

  private void showMinesLocation() {
    TxtConst.appLog.info(driver.showMineLoc());
  }
  private void createField() {
    TxtConst.appLog.info(TxtConst.CREATE_FIELD_MSG);
    try {
      String input = br.readLine();
      input.trim();
      if(input.matches("\\d+-\\d+-[0|1]\\.?\\d*")) {
        String[] fieldParams = input.split("-");
        driver.createMineField(Integer.parseInt(fieldParams[0]),
                                Integer.parseInt(fieldParams[1]),
                                Double.parseDouble(fieldParams[2]));
      }
      else {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      TxtConst.appLog.error(TxtConst.INPUT_ERR_MSG);
    } catch (IllegalArgumentException e) {
      TxtConst.appLog.warn(TxtConst.BAD_ARG_MSG);
      createField();
    }
  }
  private void exitApp() {
    System.exit(0);
  }
  private void showTxtMenu() {
    Arrays.stream(TxtConst.txtMenu)
        .map(i -> i + "\n")
        .forEach(TxtConst.appLog::info);
    TxtConst.appLog.info(TxtConst.CHOOSE_MENU_ITM_MSG);
  }
  private void processChoice(String choice) throws IOException,
                                                    FieldNotCreatedException {
    execMenu
        .entrySet()
        .stream()
        .filter(i -> i.getKey().equals(choice))
        .forEach(i -> i.getValue().execute());
    showPause();
    getChoice();
  }
  private void showPause() throws IOException{
    TxtConst.appLog.info(TxtConst.PAUSE_MSG);
    br.readLine();
  }
}
