package com.nexu6.task09.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TxtConst {
  public static final Logger appLog = LogManager.getLogger(TxtConst.class);
  public static String[] txtMenu;
  public static String INPUT_ERR_MSG = "Error reading input";
  public static String CHOOSE_MENU_ITM_MSG = "Choose menu item: ";
  public static String BAD_MENU_ITM_MSG = "Wrong menu item";
  public static String PAUSE_MSG = "Press Enter to continue";
  public static String FIELD_EXC_MSG = "Create a mine field first";
  public static String CREATE_FIELD_MSG = "Enter field dimensions, mine occurrence prob X-Y-p: ";
  public static String BAD_ARG_MSG = "Bad arguments";

  static {
    txtMenu = new String[4];
    txtMenu[0] = "1  Create mine field";
    txtMenu[1] = "2  Show mines location";
    txtMenu[2] = "3  Show game field";
    txtMenu[3] = "0  Exit program";
  }

  private TxtConst() {}
}
