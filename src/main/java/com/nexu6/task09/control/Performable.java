package com.nexu6.task09.control;

interface Performable {
  void createMineField(int x, int y, double p);
  String showMineLoc();
  String showGameView();
}
