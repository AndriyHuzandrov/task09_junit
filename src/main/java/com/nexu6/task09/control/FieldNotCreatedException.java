package com.nexu6.task09.control;

import com.nexu6.task09.utils.TxtConst;

public class FieldNotCreatedException extends RuntimeException {

    public String toString() {
      return TxtConst.FIELD_EXC_MSG;
    }
}
